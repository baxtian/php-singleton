<?php

namespace Baxtian;

trait SingletonTrait
{
	/**
	 * @var instance reference to singleton instance
	 */
	private static $instance;

	/**
	 * @var dependencies_def reference to the class definition of dependences
	 */
	protected $dependencies_def;

	/**
	 * @var dependencies_ins reference to the defined dependences
	 */
	protected $dependencies_ins = [];

	/**
	 * The Singleton's constructor should always be private to prevent direct
	 * construction calls with the `new` operator.
	 *
	 * @param array $arguments Arguments to inject. Keep empty to use the default values.
	 * @return void
	 */
	protected function __construct($arguments = [])
	{
	}

	/**
	 * Prevent the instance from being cloned.
	 *
	 * @return void
	 */
	protected function __clone()
	{
	}

	/**
	 * Prevent from being unserialized.
	 *
	 * @return void
	 */
	public function __wakeup()
	{
		throw new \Exception('Cannot unserialize a singleton.');
	}

	/**
	 * Inject dependencies and set the classes array.
	 * See README for more details.
	 * 
	 * @param array $arguments Arguments to inject. Keep empty to use the default values.
	 * @param array $classes Relations between dependencies and classes.
	 * @return mixed
	 */
	protected function set_dependencies($arguments = [], $classes = [])
	{
		$this->dependencies_def = $classes;
		foreach ($this->dependencies_def as $key => $class) {
			$this->dependencies_ins[$key] = ($arguments[$key]) ?? null;
		}
	}

	/**
	 * Gets the instance o the dependency
	 * 
	 * @param string $dependency Name of the dependency
	 * @return mixed
	 */
	private function dependency($dependency)
	{
		// Si ya tenemos valor, retornarlo
		if (!empty($this->dependencies_ins[$dependency])) {
			return $this->dependencies_ins[$dependency];
		}

		if (isset($this->dependencies_def[$dependency])) {
			$class = $this->dependencies_def[$dependency];

			// If the class uses the get_instance method, use it
			if (method_exists($class, 'get_instance')) {
				$this->dependencies_ins[$dependency] = $class::get_instance();
			} else { // if not, use class constructor intsted
				$this->dependencies_ins[$dependency] = new $class();
			}

			return $this->dependencies_ins[$dependency];
		}

		return false;
	}

	/**
	 * Gets the instance via lazy initialization (created on first usage).
	 * Use "arguments" to pass dependencies.
	 *
	 * @param array $arguments Pass dependencies to inject
	 * @return static
	 */
	public static function get_instance($arguments = [])
	{
		if (static::$instance === null) {
			static::$instance = new static($arguments);
		}

		return static::$instance;
	}
}
